# Instalação
Usando ambiente Linux MINT

```bash
crontab -e
```
Caso o comando seja executado pela primeira vez o usuário será solicitado a escolher um editor.

Será apresentado o arquivo no qual se deve escrever o comando para agendar no **CRON**.

Cole o comando abaixo, salve e feche o arquivo, se der certo o agendamento uma informação de sucesso será apresentada no terminal.
```
* * * * * cd /var/www/html/lara-agendamento && php artisan schedule:run >> /dev/null 2>&1
``` 

# Teste
No arquivo `app/Console/Kernel.php`, no método `schedule` existe um trecho de código que criará Log a cada minuto.

Sugestão de acompanhamento do Log.
```bash
tail -f storage/logs/laravel.log
```

# Info
Este exemplo **não** necessita de qualquer servidor web.
